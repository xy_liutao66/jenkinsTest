
jenkins WINDOWS构建命令

@echo off
for /f "tokens=1,2 delims= " %%a in ('jps') do (if "%%b"=="xxx.jar" (set pid=%%a))
if DEFINED  pid taskkill /pid %pid% /f


clean package -P develop -e -DskipTests



@echo off
start javaw -jar .\target\demo-0.0.1-SNAPSHOT.jar
exit

第一次需要安装maven 插件

net start jenkins
net stop jenkins


一、定时构建语法

* * * * *

(五颗星，中间用空格隔开）

第一颗*表示分钟，取值0~59
第二颗*表示小时，取值0~23
第三颗*表示一个月的第几天，取值1~31
第四颗*表示第几月，取值1~12
第五颗*表示一周中的第几天，取值0~7，其中0和7代表的都是周日

1.每30分钟构建一次：

H/30 * * * *

2.每2个小时构建一次

H H/2 * * *

3.每天早上8点构建一次

0 8 * * *

4.每天的8点，12点，22点，一天构建3次

0 8,12,22 * * *

（多个时间点，中间用逗号隔开）